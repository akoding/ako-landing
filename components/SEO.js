import React from 'react';
import Head from 'next/head';

export default ({ data }) => (
  <Head>

    <title>{data.title}</title>
    <meta name="description" content={data.description} />
    <meta name="keywords" content={data.keywords} />

  </Head>
)
