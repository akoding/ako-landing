import React from 'react';
import { useRouter } from 'next/router';

function ActiveLink({ children, href, ...props }) {
  const router = useRouter();
  const style = {
    color: router.pathname === href ? '#d0a139' : 'rgba(0,0,0,.5)',
  };

  const handleClick = (e) => {
    e.preventDefault();
    window.scrollTo(0, 0);
    router.push(href)
  };

  return (
    <a href={href} onClick={handleClick} style={style} {...props}>
      {children}
    </a>
  )
}

export default ({ children }) => {
  return (
    <>
      <header id="header" className="fixed-top">
        <div className="container-fluid d-flex">

          <div className="logo mr-auto">
            <h1 className="text-light"><a href="/"><span>AKO</span></a></h1>
          </div>

          <nav className="navbar navbar-expand-lg navbar-light navbar-header bg-light-ako">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                    aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item nav-item-header nav-item-active-header"><ActiveLink className="nav-link" href="/">AKO</ActiveLink></li>
                <li className="nav-item nav-item-header dropdown">
                  <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Pour qui ?
                  </a>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <ActiveLink className="dropdown-item" href="/particuliers">Particuliers</ActiveLink>
                    <ActiveLink className="dropdown-item" href="/independants">Indépendants</ActiveLink>
                    <ActiveLink className="dropdown-item" href="/entreprises">Entreprises</ActiveLink>
                  </div>
                </li>

                <li className="nav-item nav-item-header"><a className="nav-link" target="_blank" href="https://blog.meetako.fr">Blog</a></li>
                <li className="nav-item nav-item-header"><ActiveLink className="nav-link" href="/faq">F.A.Q</ActiveLink></li>
                <li className="nav-item nav-item-header"><a className="nav-link" href="#footer">Nous contacter</a></li>
              </ul>
            </div>
          </nav>

        </div>
      </header>

      <main id="main">
        {children}
      </main>

      <footer id="footer">

        <style jsx>{`

      .bg-mail-cta {
        background-image : url(assets/img/backgroundhero.svg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        background-attachment: fixed;
        width: 100%;
      }

      `}</style>

        <section id="hero" className="hero d-flex align-items-center p-5 m-0 bg-mail-cta h-50">

          <div className="container p-auto">
            <div className="row">

              <div id="mc_embed_signup" className="cta-email">

                <h4>Rejoindre l'AKOmmunauté</h4>
                <p>Rejoignez l'AKOmmunauté et entrez en contact avec les membres !</p>

                <form
                  action="https://meetako.us20.list-manage.com/subscribe/post?u=da789c8b55d97d2d47e788fb1&amp;id=08d2e6098f"
                  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate"
                  target="_blank" noValidate>
                  <div id="mc_embed_signup_scroll">
                    <div className="mc-field-group">
                      <input type="email" name="EMAIL" className="required email email-input"
                             placeholder="Email" id="mce-EMAIL"/>
                    </div>
                    <div id="mce-responses" className="clear">
                      <div className="response" id="mce-error-response" style={{display: 'none'}}/>
                      <div className="response" id="mce-success-response" style={{display: 'none'}}/>
                    </div>
                    <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true">
                      <input type="text"
                             name="b_da789c8b55d97d2d47e788fb1_08d2e6098f"
                             tabIndex="-1" />
                    </div>
                    <div className="clear">
                      <input type="submit" value="REJOINDRE AKO" name="subscribe"
                             id="mc-embedded-subscribe" className="btn btn-get-started submit-btn"/>
                    </div>
                  </div>
                </form>
              </div>


            </div>
          </div>
        </section>

        <div className="footer-top">
          <div className="container">
            <div className="row">

              <div className="col-lg-4 col-md-6 footer-contact" data-aos="fade-up" data-aos-delay="100">
                <h3>AKO</h3>
                <p>

                  Bordeaux<br/>
                  France <br/><br/>
                  <strong>Email:</strong> <a href="mailto:hey.akoteam@gmail.com">hey.akoteam@gmail.com</a><br/>
                </p>
              </div>

              <div className="col-lg-4 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="100">
                <h4>Liens utiles</h4>
                <ul>
                  <li><i className="bx bx-chevron-right"/> <a href="https://blog.meetako.fr" target="_blank" >Blog</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="#team">Qui sommes nous ?</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="https://blog.meetako.fr/notre-histoire" target="_blank" >Notre histoire</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="/faq">Foire aux questions</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="/particuliers">Particuliers</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="/independants">Indépendants</a></li>
                  <li><i className="bx bx-chevron-right"/> <a href="/entreprises">Entreprises</a></li>
                </ul>
              </div>


              <div className="col-lg-4 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="100">
                <h4>AKO sur les réseaux</h4>

                <div className="social-links mt-3">
                  <div className="row align-items-baseline">
                    <div className="col-2"><a href="https://twitter.com/tweets_ako" target="_blank" rel="noopener noreferer" className="twitter"><i className="bx bxl-twitter"/></a></div>
                    <div className="col-2"><p>@tweets_ako</p></div>
                  </div>

                  <div className="row align-items-baseline">
                    <div className="col-2">  <a href="https://www.facebook.com/akocourscollaboratifs/" target="_blank" rel="noopener noreferer" className="facebook"><i
                      className="bx bxl-facebook"/></a></div>
                    <div className="col-10"><p>AKO, apprentissage collaboratif</p></div>
                  </div>

                  <div className="row align-items-baseline">
                    <div className="col-2"> <a href="https://www.instagram.com/instakoteam/" target="_blank" rel="noopener noreferer" className="instagram"><i className="bx bxl-instagram"/></a></div>
                    <div className="col-10"><p>Instakoteam</p></div>
                  </div>

                  <div className="row align-items-baseline">
                    <div className="col-2"> <a href="https://www.linkedin.com/company/akoapprentissagecollaboratif/" target="_blank" rel="noopener noreferer" className="linkedin"><i
                      className="bx bxl-linkedin"/></a></div>
                    <div className="col-10"><p>AKO</p></div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

        <div className="container py-4">
          <div className="copyright">
            &copy; Copyright <strong><span>AKO</span></strong> 2020. Tous droits reservés
          </div>

        </div>
      </footer>

      <a href="#" className="back-to-top"><i className="icofont-simple-up"/></a>

    </>
  );
}
