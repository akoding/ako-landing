import React from 'react';
import Head from 'next/head'

export default () => (
  <>

    <Head>

      <meta charSet="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      <link href="favicon.ico" rel="favicon"/>
      <link href="assets/img/favicon.png" rel="favicon"/>

      <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet"/>
      <link
        href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600;700&display=swap"
        rel="stylesheet"/>

      <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
      <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet"/>
      <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet"/>
      <link href="assets/vendor/venobox/venobox.css" rel="stylesheet"/>
      <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet"/>

    </Head>

    <script src="assets/vendor/jquery/jquery.min.js" />
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js" />
    <script src="assets/vendor/jquery.easing/jquery.easing.min.js" />
    <script src="assets/vendor/php-email-form/validate.js" />
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js" />
    <script src="assets/vendor/venobox/venobox.min.js" />
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js" />

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168637005-1"/>
    <script
      dangerouslySetInnerHTML={{
        __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
    
            gtag('config', 'UA-168637005-1');
          `,
      }}
    />

  </>
)
