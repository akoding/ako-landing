import React from 'react';
import SEO from '../components/SEO';
import Layout from '../components/Layout';

const seoData = {
  title: 'AKO - Cours collaboratifs',
  description: 'Cours collaboratifs entre particuliers',
  keywords: 'ako',
};

export default () => (
  <Layout>
    <SEO data={seoData} />

    <style jsx>{`

      .hero-indep {
        background-image : url(assets/img/backgroundhero.svg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        background-attachment: fixed;
        width: 100%;
      }

      .petitp {
        font-family : quicksand;
        font-size: 0.8em;
        margin: 0;
        padding: O 0;
        line-height: 1.2em;
        font-weight: 400;
        letter-spacing: 0px;

      }


      .containerdark {
        background-color: #FAFAFA;
        border-radius: 10px;
      }

      .d-grid {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-auto-rows: 100px;
        grid-gap: 7px;
      }

      .item {
        position: relative;
      }

      .item:nth-child(1) {
        grid-column: 1 / 2;
        grid-row: 1 / 4;
      }

      .item:nth-child(2) {
        grid-column: 2;
        grid-row: 0 / 3;
      }

      .item:nth-child(3) {
        grid-column: 2;
        grid-row: 2 / 3;
      }

      .item:nth-child(4) {
        grid-column: 2;
        grid-row: 3 / 3;
      }

      .item a {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        overflow: hidden;
      }

      .item img {
        height: 100%;
        width: 100%;
        object-fit: cover;
      }

      .h3pres {
        font-weight: 400;
        font-size: 28px;
        color: #505050;
      }

      .ppres {
        font-size: 0.9em;
        color: #505050;
        font-weight: 400;
        text-align: center;
        margin-bottom: 0px;
      }

    `}</style>

    <section id="hero" className="hero d-flex align-items-center p-2 hero-indep">

      <div className="container">
        <div className="row">
          <div className="m-auto text-responsive">
            <h1>Rencontrez, apprenez, accomplissez !</h1>
            <h2> Sur AKO, troquez vos savoirs contre des heures d'apprentissage <br/> Evoluez
              rapidement et à moindre coût grâce à l'intelligence collective de notre réseau de passionnés.
            </h2>
            <div className="row justify-content-center">
              <a href="https://akoforms.typeform.com/to/PJJRHz" target="_blank"><button type="button" className="btn  btn-get-started">Expérimenter AKO</button></a>
            </div>

          </div>
        </div>
      </div>

    </section>


    <div className="container containerdark my-5 py-2 z-depth-1">


      <section className="text-center dark-grey-text">

        <h3 className="h3pres mb-4 pb-2">Ne cessez jamais d'apprendre !</h3>
        <p className="lead ppres mx-auto mb-5">
          <br/>Le monde d'aujourd'hui, en perpétuelle évolution, nous demande d'être toujours plus efficace, adapté et polyvalent.
          <br/> Dans ce contexte, AKO a pour ambition de révolutionner votre apprentissage.
          <br/> A l'heure où des milliers de manières d'apprendre existent, avez-vous trouvé celle qui vous correspond vraiment ?
        </p>

      </section>

    </div>


    <section id="about-1" className="about">
      <div className="container">
        <div className="row justify-content-between vertical-align-baseline p-2">
          <div className="col-lg-6 d-flex m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/particuliersv1.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 py-5 m-auto justify-content-center">
            <h3 className="right" data-aos="fade-up">Mutualiser les savoirs et <br/>populariser l'apprentissage<span className="color-gold">.</span><br/></h3>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">
              <br/>

              Faire bénéficier des diversités d'intelligences humaines pour permettre à une communauté de se développer collectivement
              <br/>grâce à un apprentissage actif, accessible et humain.
              <br/> Rendre l'apprentissage actif et personnalisé accessible à tous.
              <br/><span className="font-weight-bold">Profite d'un apprentissage basé sur un échange bienveillant.</span>
            </p>
          </div>
        </div>
      </div>
    </section>

    <section id="about-2" className="about">
      <div className="container" >
        <div className="row justify-content-between d-flex p-2 flex-wrap-reverse">

          <div className="col-lg-6 m-auto col-md-pull-6 justify-content-center py-5">
            <h3 data-aos="fade-up">Construire un apprentissage sur mesure et adapté à chacun<span className="color-gold">.</span></h3>
            <p className="pleft" data-aos="fade-up" data-aos-delay="100">

              <br/> Compétence, personnalité, ton de la session, localisation, urgence de la demande, mode de communication... chacun à ses préférences d'apprentissage parce que chacun est différent.
              <br/>AKO a l'ambition être un vrai copilote et guider chaque personne dans son apprentissage, de manière pertinente, utile et intelligente.
              <span className="font-weight-bold"><br/>Viens avec un projet, AKO se charge du reste ! </span>
            </p>
          </div>

          <div className="col-lg-6 d-flex align-items-center justify-content-center about-img">
            <img src="assets/img/indexv2.png" className="img-fluid" alt="" data-aos="zoom-in"/>
          </div>

        </div>
      </div>

    </section>


    <section id="about-3" className="about">
      <div className="container">
        <div className="row justify-content-between vertical-align-baseline p-2">
          <div className="col-lg-6 d-flex m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/indexV3.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 py-5 m-auto justify-content-center">
            <h3 className="right" data-aos="fade-up">Révéler et mettre en valeur la diversité des talents humains<span className="color-gold">.</span><br/></h3>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">
              <br/>
              AKO a l'ambition d'être le témoin des expériences de projets professionnels et personnels de chacun pour les rendre légitimes auprès des institutions.
              <span className="font-weight-bold"><br/>Partage ton expertise et met en valeur tes talents !</span>
            </p>
          </div>
        </div>
      </div>
    </section>

    <section id="app" className="app">
      <div className="container">
        <div className="column justify-content-center vertical-align-baseline p-2">

          <div>
            <h3 className="centerh3 " data-aos="flip-left">Tout ceci dans une application !
            </h3>
            <p className="pcenter" data-aos="fade-up" data-aos-delay="100">
              <br/>
              <br/> Quelques étapes simples à suivre pour apprendre efficacement grâce à un mentor engagé et bienveillant.
              <br/> Inscris toi sur AKO, gagne un AKOin et bénéficies d'une session de cours offerte.
              <br/>Profite de cette session pour commencer à réaliser tes projets.
              <br/> Aide à ton tour un membre pour gagner d'autres sessions auprès de l'AKOmmunauté.

              <br/>  <span className="font-weight-bold">Donne un cours pour prendre un cours !</span>
            </p>
          </div>

          <div className="col-lg-10 d-flex p-4 m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/devicesnew.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>

        </div>
      </div>

      <div className="services section-bg">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icon"><i className='bx bx-log-in-circle' /></div>
                <h4 className="title"><a href="">1 - Inscris-toi facilement sur AKO</a></h4>
                <p className="description"> Rejoins la communauté d'entraide. Renseigne tes besoins et tes préférences d'apprentissage.
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Tu gagnes un AKOin. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icon"><i className='bx bx-user-check'/></div>
                <h4 className="title"><a href="">2 - Effectue ta recherche simplement</a></h4>
                <p className="description">Trois matchs parfaits te seront instantanément proposés. Tu n'as plus qu'à choisir lequel te convient le mieux !​
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Une session coûte un AKOin. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icon"><i className='bx bx-edit-alt'/></div>
                <h4 className="title"><a href="">3 - Réalise tes projets et apprends </a></h4>
                <p className="description">Utilise cette session privilégiée pour travailler sur ton projet, avec l'aide d'un mentor adapté et bienveillant.
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Ton AKOin lui est transféré. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icon"><i className='bx bx-user-voice' /></div>
                <h4 className="title"><a href="">4 - Transmets tes compétences</a></h4>
                <p className="description">Guide la communauté, bénéficie de nouvelles heures d'apprentissage et fais évoluer ton avatar !
                  <br/><br/><br/><span className="font-weight-bold align-items-end" style={{color: "#C5C5C5"}}> Récolte d'autres AKOins ! </span>
                </p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>


    <div className="container pt-5 my-5 z-depth-1">
      <section className="p-md-3 mx-md-5 text-center text-lg-left">
        <h3 className="h3pres text-center mx-auto mb-4 pb-2">AKO, pour qui ?</h3>
        <div className="row">

          <div className="col-lg-4 mb-4 iconbis">
            <div className="p-4">
              <a href="/particuliers">
                <div className="avatar w-100 white d-flex justify-content-center align-items-center">

                  <img
                    src="/assets/img/part.jpg"
                    className="img-fluid rounded-circle z-depth-1"
                  />
                </div>
                <div className="text-center mt-3">
                  <h6 className="font-weight-bold pt-2">Pour les particuliers</h6>
                  <p className="text-muted">
                    Apprenez grâce à notre communauté de passionnés, simplement
                  </p>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-4 mb-4 iconbis">
            <div className="p-4">
              <a href="/independants">
                <div
                  className="avatar w-100 white d-flex justify-content-center align-items-center"
                >
                  <img
                    src="/assets/img/indep.jpg"
                    className="img-fluid rounded-circle z-depth-1"
                  />
                </div>
                <div className="text-center mt-3">
                  <h6 className="font-weight-bold pt-2">Pour les indépendants</h6>
                  <p className="text-muted">
                    Apprenez et diversifiez vos compétences grâce à votre nouveau réseau
                  </p>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-4 mb-4 iconbis">
            <div className="p-4">
              <a href="/entreprises">
                <div
                  className="avatar w-100 white d-flex justify-content-center align-items-center"
                >
                  <img
                    src="/assets/img/business.jpg"
                    className="img-fluid rounded-circle z-depth-1"
                  />
                </div>
                <div className="text-center mt-3">
                  <h6 className="font-weight-bold pt-2">Pour les entreprises</h6>
                  <p className="text-muted">
                    Révélez et faites collaborer les talents internes à votre entreprise
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>

    <section className="hero d-flex align-items-center p-5 m-0 hero-indep h-50">

      <div className="container p-auto">
        <div className="row">

        <section className="hero d-flex align-items-center p-5 m-0 hero-indep h-50">

          <div className="container p-auto">

            <div className="row justify-content-center">
              <a href="https://akoforms.typeform.com/to/PJJRHz" target="_blank"><button type="button" className="btn  btn-get-started">Expérimenter AKO</button></a>
            </div>

          </div>

        </section>


        </div>
      </div>

    </section>

    <section className="dark-grey-text">
      <div className="container my-5 px-5 z-depth-1">


        <div className="row align-items-center">

          <div className="col-lg-5 mb-4">
            <h3 className=" mb-4">L'AKOLab'</h3>
            <p className="lead text-muted">Découvrez les ateliers collaboratifs d'AKO !</p>
            <p className="text-muted">Des ateliers qui vous permettent de rencontrer l'AKOmmunauté autour d'une session d'apprentissage.
              <br/>Ambiance conviviale et bienveillante garantie !</p>

            <a href="https://www.facebook.com/akocourscollaboratifs/" target="_blank"><button type="button" className="btn btndiscover">Rejoindre AKO sur Facebook</button></a>



          </div>

          <div className="col-lg-7 mb-4">

            <div id="mdb-lightbox-ui"></div>

            <div className="d-grid mdb-lightbox z-depth-1 ">
              <figure className="item">
                <img src="assets/img/ateliers/marjoemmanuelle.JPG" className="rounded"/>
              </figure>

              <figure className="item">
                <img src="assets/img/ateliers/paconathalie.JPG" className="rounded"/>
              </figure>

              <figure className="item">
                <img src="assets/img/ateliers/everybody.JPG" className="rounded"/>
              </figure>

              <figure className="item">
                <img src="assets/img/ateliers/audreykevin.JPG" className="rounded"/>
              </figure>
            </div>

          </div>

        </div>
      </div>
    </section>

    <section id="testimonials">
      <div className="testimonial6">

        <div className="container">
          <div className="section-title" data-aos="fade-up">
            <h3>Nous avons testé AKO !</h3>
          </div>
          <div className="row">

            <div className="col-lg-2 col-md-3">

              <div className="nav flex-column nav-pills testi6" id="v-pills-tab" role="tablist">
                <a className="nav-link active" data-toggle="pill" href="#t6-1" role="tab" aria-controls="t6-1" aria-expanded="true">
                  <img src="assets/img/leo.jpg" alt="wrapkit" className="rounded-circle border-radius-10" />
                </a>
                <a className="nav-link" data-toggle="pill" href="#t6-2" role="tab" aria-controls="t6-2" aria-expanded="true">
                  <img src="assets/img/marjojo.jpg" alt="wrapkit" className="rounded-circle" />
                </a>
                <a className="nav-link" data-toggle="pill" href="#t6-3" role="tab" aria-controls="t6-3" aria-expanded="true">
                  <img src="assets/img/audrey.jpg" alt="wrapkit" className="rounded-circle" />
                </a>
                <a className="nav-link" data-toggle="pill" href="#t6-4" role="tab" aria-controls="t6-4" aria-expanded="true">
                  <img src="assets/img/olga.jpg" alt="wrapkit" className="rounded-circle" />
                </a>
              </div>
            </div>
            <div className="col-lg-9 col-md-8 ml-auto align-self-center">
              <div className="tab-content" id="v-pills-tabContent">
                <div className="tab-pane fade show active" id="t6-1" role="tabpanel">

                  <p className=" subtitle mt-4 font-weight-normal">Super projet, une bonne convivialité et de belles rencontres aussi bien humaines que professionnelles, à ne pas louper !</p>
                  <div className="d-flex mt-5">
                    <div>


                      <h5 className="mb-0 text-uppercase font-weight-normal">Léo A.</h5>

                      <h3pres className="subtitle font-weight-normal">Bordeaux</h3pres>
                      <a href="https://www.linkedin.com/in/l%C3%A9o-arias-6626a2198/" target="_blanck">
                        <br/><i className='bx bxl-linkedin-square bx-sm'/></a>
                    </div>

                  </div>
                </div>
                <div className="tab-pane fade" id="t6-2" role="tabpanel">

                  <p className="subtitle mt-4 font-weight-normal">Grâce à AKO j'ai pu trouver l'aide qu'il me fallait pour réaliser des tableaux croisés dynamiques sur Excel et analyser de manière plus approfondie des données marketing pour un projet. Maintenant que je sais faire, je peux répondre à plus d'annonces !</p>
                  <div className="d-flex mt-5">
                    <div>
                      <h5 className="mb-0 text-uppercase font-weight-normal">Marjorie B.</h5>
                      <h3pres className=" subtitle font-weight-normal">Bordeaux</h3pres>
                      <a href="https://www.linkedin.com/in/acquisition-manager/" target="_blanck">
                        <br/><i className='bx bxl-linkedin-square bx-sm'/></a>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="t6-3" role="tabpanel">

                  <p className="subtitle mt-4 font-weight-normal">J'ai aidé une illustratrice pour un projet de dessin d'architecture pendant deux heures et j'ai beaucoup appris sur moi-même. Ca m'a permis de me replonger dans les aspects techniques et d'approfondir ma méthode pédagogique ! On reste en contact et j'espère que l'on pourra collaborer bientôt.</p>
                  <div className="d-flex mt-5">
                    <div>
                      <h5 className="mb-0 text-uppercase font-weight-normal">Audrey P.</h5>
                      <h3pres className=" subtitle font-weight-normal">Paris</h3pres>
                      <a href="https://www.linkedin.com/in/audrey-pradeau-ako/" target="_blanck">
                        <br/><i className='bx bxl-linkedin-square bx-sm'/></a>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="t6-4" role="tabpanel">

                  <p className="subtitle mt-4 font-weight-normal">Quand avec l’équipe AKO nous devions réaliser une étude de marché et interviewer nos futurs clients, je ne savais pas comment m’y prendre et j’avais trop peur d’interviewer des inconnus.

                    C’est là où j’ai décidé de profiter de l’expérience AKO et je me suis fait aidée par Marjorie.
                    <br/>Elle m’a d’abord aidé à préparer les questions pour mes interviews semi-dirigées. Ensuite, nous avons fait une simulation d’un vrai entretien : je lui posait mes questions et elle me répondait. Ainsi j’ai pu voir que j’arrivait à orienter l’interview en fonction de ses réponses et je n’avais plus peur de faire les vrais entretiens !</p>
                  <div className="d-flex mt-5">
                    <div>
                      <h5 className="mb-0 text-uppercase font-weight-normal">Olga F.</h5>
                      <h3pres className=" subtitle font-weight-normal">Bordeaux</h3pres>
                      <a href="https://www.linkedin.com/in/olgafazylova/" target="_blanck">
                        <br/><i className='bx bxl-linkedin-square bx-sm'/></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>



    <section id="team" className="team">
      <div className="container">

        <div className="section-title" data-aos="fade-up">
          <h3>L'équipe</h3>
        </div>

        <div className="row mt-3">
          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">

            <img src="assets/img/team/team.png" className="img-fluid" alt=""/>


          </div>

          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
            <div className="member">
              <img src="assets/img/team/team-1.jpg" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Audrey Pradeau</h4>
                  <span>CEO</span>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
            <div className="member">
              <img src="assets/img/team/team-2.jpg" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Olga Fazylova</h4>
                  <span>COO</span>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
            <div className="member">
              <img src="assets/img/team/team-3.jpg" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Tristan Pinaudeau</h4>
                  <span>DevOps</span>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
            <div className="member">
              <img src="assets/img/team/team-4.jpg" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Pacien Boisson</h4>
                  <span>Lead developer</span>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-2 col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
            <div className="member">
              <img src="assets/img/team/team-5.png" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Leslie Madeleine Torrigiani</h4>
                  <span>Chargée de communication</span>
                </div>
              </div>
            </div>
          </div>



        </div>

      </div>
    </section>


    <section className="py-5">
      <div className="container z-depth-1 my-5">



        <h3 className=" text-center">Ils croient en nous</h3>

        <div className="row mt-3">

          <div className="col-4 p-3">
            <div className="view">
              <a href="https://na.wikilespremieres.com/fr" target="_blank"><img className="img-fluid mx-auto imgsize" src="assets/img/premieres.jpg" alt="Les Premières NA"/>
                <p className="pcenter p-3"> Les Premières <br/>Nouvelle Aquitaine </p>
              </a>
            </div>
          </div>
          <div className="col-4 p-3">
            <div className="view">
              <a href="https://lereacteur.io" target="_blank"><img className="img-fluid mx-auto imgsize" src="assets/img/reacteur.png" alt="Le réacteur"/>
                <p className="pcenter p-3"> Le Réacteur </p>
              </a>
            </div>
          </div>
          <div className="col-4 p-3">
            <div className="view">
              <a href="https://makesense.org/" target="_blank"> <img className="img-fluid mx-auto imgsize" src="assets/img/makesense.png" alt="Make Sense"/>
                <p className="pcenter p-3"> MakeSense </p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>


  </Layout>
)
