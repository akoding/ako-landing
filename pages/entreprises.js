import React from 'react';
import SEO from '../components/SEO';
import Layout from '../components/Layout';

const seoData = {
  title: 'AKO - Cours collaboratifs',
  description: 'Cours collaboratifs entre particuliers',
  keywords: 'ako',
};

export default () => (
  <Layout>
    <SEO data={seoData} />

    <style jsx>{`


      .hero-indep {
        background-image : url(assets/img/backgroundentreprises.svg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        background-attachment: fixed;
        width : 100%;
      }

      .title {
        font-size: 18px;
        font-weight: 700;
        margin-top: 10px;
        color: #505050;
        text-align: center;
      }

      .ientreprises {
        font-size: 48px;
        line-height: 1;
        margin-bottom: 15px;
        color: #303960;
      }

        .petitp {
          font-family : quicksand;
          font-size: 0.8em;
          margin: 0;
          padding: O 0;
          line-height: 1.2em;
          font-weight: 400;
          letter-spacing: 0px;

        }


        .containerdark {
          background-color: #363B59;
          border-radius: 10px;
        }

        .d-grid {
          display: grid;
          grid-template-columns: repeat(2, 1fr);
          grid-auto-rows: 100px;
          grid-gap: 7px;
        }

        .item {
          position: relative;
        }

        .item:nth-child(1) {
          grid-column: 1 / 2;
          grid-row: 1 / 4;
        }

        .item:nth-child(2) {
          grid-column: 2;
          grid-row: 0 / 3;
        }

        .item:nth-child(3) {
          grid-column: 2;
          grid-row: 2 / 3;
        }

        .item:nth-child(4) {
          grid-column: 2;
          grid-row: 3 / 3;
        }

        .item a {
          position: absolute;
          left: 0;
          right: 0;
          bottom: 0;
          top: 0;
          overflow: hidden;
        }

        .item img {
          height: 100%;
          width: 100%;
          object-fit: cover;
        }


        .btn-get-startedbis {

          font-family: "quicksand", "roboto", sans-serif;
          font-weight: 500;
          font-size: 1em;
          letter-spacing: 1px;
          display: inline-block;
          padding: 8px 28px;
          border-radius: 30px;
          height: 42px;
          transition: 0.5s;
          margin: auto;
          color: #fff;
          background: #F9BC32;
          border-color: #F9BC32;
          border-width: 0px;
        }

        .btn-get-startedbis:hover {
          background: #fff;
          color: #F9BC32;
        }

    `}</style>

    <section id="hero" className="hero d-flex align-items-center p-2 hero-indep">

      <div className="container">
        <div className="row">
          <div className="m-auto text-responsive">
            <h1>Collaborez, apprenez, accomplissez !</h1>
            <h2> Formez vos collaborateurs grâce aux talents présents au sein de votre entreprise ! <br/>Révélez et universalisez les savoirs en interne grâce au travail collaboratif.
            </h2>
            <div className="row justify-content-center">
              <a href="https://akoforms.typeform.com/to/bex43s" target="_blank"><button type="button" className="btn  btn-get-started">Intégrer AKO dans mon entreprise</button></a>
            </div>
          </div>

        </div>
      </div>

    </section>


    <div className="container containerdark my-5 py-2 z-depth-1">


      <section className="text-center dark-grey-text">

        <h3 className="h3pres mb-4 pb-2">Ne cessez jamais d'apprendre !</h3>
        <p className="lead ppres mx-auto mb-5">Le monde d'aujourd'hui, en perpétuelle évolution, nous demande d'être toujours plus efficace, adapté et polyvalent.
          <br/>Dans ce contexte, AKO s'est donné les missions suivantes :
        </p>

        <div className="row">

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-world"/>
              <p className="pj font-weight-bold my-4">Mutualiser les savoirs et <br/>populariser l'apprentissage</p>
            </div>

          </div>

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-user-check"></i>
              <p className="pj font-weight-bold my-4">Construire un apprentissage sur mesure et adapté à chacun</p>
            </div>

          </div>

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-bulb"></i>
              <p className="pj font-weight-bold my-4">Révéler et mettre en valeur la diversité des talents humains </p>
            </div>

          </div>

        </div>

      </section>


    </div>




    <section id="about-1" className="about">
      <div className="container">
        <div className="row justify-content-between vertical-align-baseline p-2">
          <div className="col-lg-6 d-flex m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/entreprisev1.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 py-5 m-auto justify-content-center">
            <h3 className="right" data-aos="fade-up">Révélez et gérez efficacement <br/>les talents présents en interne<span className="color-gold">.</span></h3>
            <br/>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">

              <span className="font-weight-bold">Grâce à notre suivi dynamique des compétences, proposez au bon moment des formations adaptées et pilotez vos stratégies de développement pour anticiper les challenges de demain.</span> <br/>
              <br/>Intégrez naturellement les nouveaux arrivants au sein de votre entreprise. Orientez les vers les sachants. En parallèle, valorisez vos profils experts en leur offrant le rôle de mentor.

            </p>
          </div>
        </div>
      </div>
    </section>



    <section id="about-2" className="about">
      <div className="container" >
        <div className="row justify-content-between d-flex p-2 flex-wrap-reverse">

          <div className="col-lg-5 pt-5 col-md-pull-6">
            <h3 data-aos="fade-up">Gagnez du temps grâce à <br/>l'apprentissage actif entre pairs<span className="color-gold">.</span></h3>
            <p className="pleft" data-aos="fade-up" data-aos-delay="100">
              <br/>L'algorithme d'AKO permet de matcher en temps réel des profils de travail compatibles.
              Lors d'une difficulté dans l'accomplissement de son travail, le collaborateur peut instantanément trouver un collègue qui saura le guider.
              <br/><span className="font-weight-bold">L'apprenant et le sachant apprennent activement tout au long de la réalisation du projet.</span>


            </p>
          </div>

          <div className="col-lg-6 d-flex align-items-center justify-content-center about-img">
            <img src="assets/img/entreprisev2.png" className="img-fluid" alt="" data-aos="zoom-in"/>
          </div>

        </div>
      </div>

    </section>

    <section id="about-3" className="about">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-lg-5 d-flex align-items-center justify-content-center about-img">
            <img src="assets/img/entreprisev3.png" className="img-fluid" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 pt-5">
            <h3 className="right" data-aos="fade-up">Renforcez la collaboration <br/>et la culture d'apprentissage<span className="color-gold">.</span></h3>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">
              <br/>
              Le feedback bienveillant laissé après chaque session d'apprentissage permet aux collaborateurs de progresser collectivement et permet de travailler respectivement sur des axes d'amélioration.

              <br/>
              <br/><span className="font-weight-bold">Rejoindre AKO c'est permettre aux collaborateurs de se réunir dans toute la diversité de leurs métiers, parcours, cultures et générations !</span>
            </p>
          </div>
        </div>
      </div>

    </section>



    <section id="services" className="services section-bg">
      <div className="container">


        <div className="container">
          <div className="column justify-content-center vertical-align-baseline p-2">

            <div>
              <h3 className="centerh3" data-aos="flip-left">Tout ceci au sein d'une même plateforme !
              </h3>

            </div>

            <div className="col-lg-10 d-flex p-4 m-auto align-items-center justify-content-center about-img">
              <img src="assets/img/devicespro.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
            </div>

          </div>
        </div>

        <div className="section-title" data-aos="fade-up">

          <h5>Formez vos collaborateurs grâce à l'apprentissage entre pairs</h5>
        </div>



        <div className="row">

          <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div className="icon-box">
              <div className="icon"><i className='bx bx-log-in-circle' /></div>
              <h4 className="title"><a href="">1 - Accédez au workspace privé sur AKO</a></h4>
              <p className="description">Les collaborateurs remplissent simplement leur profil.
              </p>
            </div>
          </div>

          <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div className="icon-box">
              <div className="icon"><i className='bx bx-user-check' /></div>
              <h4 className="title"><a href="">2 - L'apprenant trouve instantanément un sachant</a></h4>
              <p className="description">quand il rencontre un point bloquant sur son projet.
              </p>
            </div>
          </div>

          <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div className="icon-box">
              <div className="icon"><i className='bx bx-edit-alt'/></div>
              <h4 className="title">3 -La session d'apprentissage entre pairs est organisée</h4>
              <p className="description">durant laquelle le travail est efficacement fait.</p>
            </div>
          </div>

          <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div className="icon-box">
              <div className="icon"><i className='bx bx-user-voice' /></div>
              <h4 className="title">4 - Un feedback bienveillant est récolté</h4>
              <p className="description">dans l'objectif d'une progression commune. </p>
            </div>
          </div>

        </div>
      </div>
    </section>


    <section id="faq" className="faq section-bg">
      <div className="container">

        <div className="section-title" data-aos="fade-up">
          <h2>F.A.Q.</h2>
          <p>Questions les plus posées</p>
        </div>

        <ul className="faq-list">

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" className="" href="#faq1">Que signifie AKO ?<i className="icofont-simple-up"/></a>
            <div id="faq1" className="collapse show" data-parent=".faq-list">
              <p>
                Ako signifie apprendre et enseigner en maori.
                <br/>Selon le concept d'ako:
                <br/>- l'apprenant et l'enseignant peuvent tous les deux apporter des connaissances durant les interactions d'apprentissage
                <br/>- de nouvelles connaissances naissent grâce à l'apprentissage partagé
                <br/>- on n'attend pas des enseignants la maîtrise parfaite de tout
                <br/>Nous partageons pleinement ces principes et nous voulons construire grâce à AKO une communauté d'apprentissage bienveillante et inclusive. Une communauté où la contribution de chacun sera valorisée. Une communauté où chacun pourra profiter des relations productives pour apprendre avec et auprès des autres membres.
              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq2" className="collapsed">Comment mettre AKO en place dans mon entreprise ? <i className="icofont-simple-up"/></a>
            <div id="faq2" className="collapse" data-parent=".faq-list">
              <p>
                La plateforme AKO est actuellement en phase de construction. Vous pouvez devenir ses premiers testeurs avec un tarif symbolique.
                <br/>En vous inscrivant sur AKO, un workspace dédié sera créé pour votre entreprise. Les collaborateurs auront accès à leurs comptes personnels et pourront commencer à apprendre dès le premier jour. Les managers et les RH auront également accès au suivi dynamique des compétences de leurs équipes.
                <br/>AKO vous accompagnera tout au long de ce test et nous pourrons imaginer ensemble les fonctionnalités sur mesure.              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq3" className="collapsed">Dans mon entreprise, nous avons déjà mis en place des initiatives comme des cercles pratiques. Comment AKO peut-il nous être utile ? <i className="icofont-simple-up"/></a>
            <div id="faq3" className="collapse" data-parent=".faq-list">
              <p>
                AKO ne vient pas remplacer les solutions existantes dans votre entreprise, AKO vient les compléter.
                <br/>Notre plateforme encourage les collaborateurs à échanger leurs compétences et les bonnes pratiques dans une ambiance bienveillante, de manière volontaire et quand le besoin immédiat existe.
                <br/>Avec AKO, il est possible de capitaliser sur les apprentissages réalisés. Pourquoi ne pas organiser vos cercles pratiques avec AKO ?
              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq4" className="collapsed">Dans mon entreprise, nous manquons déjà de temps pour la formation. Avec AKO, nos experts risquent d'être sursollicités, n'est-ce pas ? <i className="icofont-simple-up"/></a>
            <div id="faq4" className="collapse" data-parent=".faq-list">
              <p>
                Si vous manquez de temps pour apprendre avec les solutions traditionnelles, AKO est une solution idéale pour vous. Toute la philosophie se construit autour d'un apprentissage pratique sur des vrais projets.
                Une heure de session d'apprentissage avec AKO correspond à une heure de travail collaboratif qui fera avancer le projet et une heure d'apprentissage pours les pairs engagés.
                <br/>Concernant les experts : ils partageront leur expertise avec leurs collègues, ce qui sera au contraire, bénéfique pour l'entreprise. Plus de collaborateurs maîtriseront les concepts difficiles.               </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq5" className="collapsed">Comment assurer la qualité des formations données avec AKO ? Qui peut s'appeler expert ? Tout le monde n'est pas pédagogue.<i className="icofont-simple-up"/></a>
            <div id="faq5" className="collapse" data-parent=".faq-list">
              <p>
                Les compétences des collaborateurs seront recommandées par leurs pairs ce qui fera preuve de l'expertise des membres actifs.
                <br/>En apprenant et en partageant leurs savoirs en parallèle de leurs projets, les collaborateurs pourront gagner en expertise.
                <br/>Quant à la pédagogie, nous sommes persuadés que cela s'apprend : à l'aide du feedback bienveillant et de la pratique, les collaborateurs apprennent progressivement à transmettre leurs connaissances de manière claire.
                <br/>Par ailleurs, nous préparons des modules de formations  pour nos utilisateurs pour les aider à partager leurs savoirs efficacement.              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq6" className="collapsed">J'aimerais contribuer au projet AKO, comment faire ? <i className="icofont-simple-up"/></a>
            <div id="faq6" className="collapse" data-parent=".faq-list">
              <p>
                Dès le début du projet, nous co-construisons AKO avec ses utilisateurs potentiels. Nous sommes toujours à l'écoute de vos suggestions et serons ravis de profiter de votre aide occassionnelle pour avoir un retour sur nos idées et réalisations.
                Si vous voulez échanger, n'hésitez pas à nous écrire sur hey.akoteam@google.com.
                <br/>Si vous voulez contribuez de manière plus sérieuse et engagée au projet AKO, nous serions ravis d'étudier votre candidature.
                <br/>Dans ce cas là, envoyez-nous votre CV et quelques lignes présentant vos motivations sur notre mail hey.akoteam@google.com en indiquant "Je veux rejoindre l'équipe AKO" dans l'objet de votre message.              </p>
            </div>
          </li>

        </ul>

      </div>
    </section>

  </Layout>
)
