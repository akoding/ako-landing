import React from 'react';
import SEO from '../components/SEO';
import Layout from '../components/Layout';

const seoData = {
  title: 'AKO - Cours collaboratifs',
  description: 'Cours collaboratifs entre particuliers',
  keywords: 'ako',
};

export default () => (
  <Layout>
    <SEO data={seoData} />

    <style jsx>{`


      .hero-indep {
        background-image : url(assets/img/backgroundhero.svg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
      }




      .petitp {
        font-family : quicksand;
        font-size: 0.8em;
        margin: 0;
        padding: O 0;
        line-height: 1.2em;
        font-weight: 400;
        letter-spacing: 0px;

      }


      .containerdark {
        background-color: #363B59;
        border-radius: 10px;
      }

      .d-grid {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-auto-rows: 100px;
        grid-gap: 7px;
      }

      .item {
        position: relative;
      }

      .item:nth-child(1) {
        grid-column: 1 / 2;
        grid-row: 1 / 4;
      }

      .item:nth-child(2) {
        grid-column: 2;
        grid-row: 0 / 3;
      }

      .item:nth-child(3) {
        grid-column: 2;
        grid-row: 2 / 3;
      }

      .item:nth-child(4) {
        grid-column: 2;
        grid-row: 3 / 3;
      }

      .item a {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        overflow: hidden;
      }

      .item img {
        height: 100%;
        width: 100%;
        object-fit: cover;
      }

      .icongold {
        color: #FBCA64;
        font-size: 48px;
        line-height: 1;
        margin-bottom: 15px;
      }

    `}</style>

    <section className="hero d-flex align-items-center p-2 hero-indep">

      <div className="container">
        <div className="row">
          <div className="m-auto text-responsive">
            <h1>Rencontrez, apprenez, accomplissez !</h1>
            <h2> Sur AKO, troque tes savoirs contre des heures d'apprentissage <br/> Evolue
              rapidement et à moindre coût grâce à l'intelligence collective de notre réseau de passionnés.
            </h2>
            <div className="row justify-content-center">
              <a href="https://akoforms.typeform.com/to/PJJRHz" target="_blank"><button type="button" className="btn  btn-get-started">Expérimenter AKO</button></a>
            </div>
          </div>

        </div>
      </div>

    </section>


    <div className="container containerdark my-5 py-2 z-depth-1">


      <section className="text-center dark-grey-text">

        <h3 className="h3pres mb-4 pb-2">Ne cessez jamais d'apprendre !</h3>
        <p className="lead ppres mx-auto mb-5">Le monde d'aujourd'hui, en perpétuelle évolution, nous demande d'être toujours plus efficace, adapté et polyvalent.
          <br/>Dans ce contexte, AKO s'est donné les missions suivantes :
        </p>

        <div className="row">

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-world"/>
              <p className="pj font-weight-bold my-4">Mutualiser les savoirs et <br/>populariser l'apprentissage</p>
            </div>

          </div>

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-user-check"></i>
              <p className="pj font-weight-bold my-4">Construire un apprentissage sur mesure et adapté à chacun</p>
            </div>

          </div>

          <div className="col-xl-4 col-md-4 mb-4">
            <div className="icon-box iconbis">
              <i className="bx bx-md bx-bulb"></i>
              <p className="pj font-weight-bold my-4">Révéler et mettre en valeur la diversité des talents humains </p>
            </div>

          </div>

        </div>

      </section>


    </div>




    <section id="about-1" className="about">
      <div className="container">
        <div className="row justify-content-between vertical-align-baseline p-2">
          <div className="col-lg-6 d-flex m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/particuliersv1.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 py-5 m-auto justify-content-center">
            <h3 className="right" data-aos="fade-up">Rencontre des passionnés comme toi<span className="color-gold">.</span><br/></h3>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">
              <br/>
              Profite de l'impulsion collective pour apprendre et dépasser tes objectifs.
              <br/> Fais toi accompagner par une communauté soudée dans ton apprentissage.
              <br/>
              <br/>Tu n'as pas envie d'aller courir ?
              <br/>Trouve alors un coach qui saura te motiver sur le long terme.
              <br/> Grâce au matching des profils, tu pourras expérimenter de nouvelles compétences à l'aide d'un mentor adapté.
              <br/><span className="font-weight-bold">Viens avec ta curiosité, AKO se charge du reste !</span>
            </p>
          </div>
        </div>
      </div>
    </section>

    <section id="about-2" className="about">
      <div className="container" >
        <div className="row justify-content-between d-flex p-2 flex-wrap-reverse">
          <div className="col-lg-6 m-auto col-md-pull-6 justify-content-center py-5">
            <h3 data-aos="fade-up">Profite d'un apprentissage par l'expérience<span className="color-gold">.</span></h3>
            <p className="pleft" data-aos="fade-up" data-aos-delay="100">
              <br/>Donne-toi l'opportunité d'être actif dans ton apprentissage : tu apprends mieux en faisant par toi-même.
              <br/><span className="font-weight-bold">Finis, les tutos que tu ne termines jamais !</span>
              <br/><br/>Rencontre des passionnés inspirants qui sauront te motiver et t'accompagner dans ta démarche, afin de faire tes propres expériences.
            </p>
          </div>
          <div className="col-lg-6 d-flex align-items-center justify-content-center about-img">
            <img src="assets/img/particuliersv2bis.png" className="img-fluid" alt="" data-aos="zoom-in"/>
          </div>
        </div>
      </div>
    </section>


    <section id="about-3" className="about">
      <div className="container">
        <div className="row justify-content-between vertical-align-baseline p-2">
          <div className="col-lg-6 d-flex m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/particuliersv3.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>
          <div className="col-lg-6 py-5 m-auto justify-content-center">
            <h3 className="right" data-aos="fade-up">Fais valoir tes talents<span className="color-gold">.</span><br/></h3>
            <p className="pright" data-aos="fade-up" data-aos-delay="100">
              <br/>
              <span className="font-weight-bold">Découvre tes capacités et sois un véritable guide pour l'AKOmmunauté !</span>
              <br/>
              <br/>Tu es un cuisinier émérite, une bricoleuse sans défauts, un polyglotte discret ?
              <br/>Il y aura toujours une personne prête à recevoir ton aide pour progresser.
              <br/>
              <span className="font-weight-bold"> Partage publiquement tes succès.</span>
              <br/>
              <br/>Enrichis ton profil de tes projets personnels et sur lesquels tu es intervenu. Qui sait, tu trouveras peut-être de nouvelles opportunités professionnelles  !
            </p>
          </div>
        </div>
      </div>
    </section>

    <section className="hero d-flex align-items-center p-5 m-0 hero-indep h-50">

      <div className="container p-auto">

        <div className="row justify-content-center">
          <a href="https://akoforms.typeform.com/to/PJJRHz" target="_blank"><button type="button" className="btn  btn-get-started">Expérimenter AKO</button></a>
        </div>

      </div>

    </section>

    <section id="app" className="app">
      <div className="container">
        <div className="column justify-content-center vertical-align-baseline p-2">

          <div>
            <h3 className="centerh3 font-weight-bold" data-aos="flip-left">Tout ceci dans une application !
            </h3>
            <p className="pcenter" data-aos="fade-up" data-aos-delay="100">
              <br/>
              <br/> Quelques étapes simples à suivre pour apprendre efficacement grâce à un mentor engagé et bienveillant.
              <br/> Inscris toi sur AKO, gagne un AKOin et bénéficies d'une session de cours offerte.
              <br/>Profite de cette session pour commencer à réaliser tes projets.
              <br/> Aide à ton tour un membre pour gagner d'autres sessions auprès de l'AKOmmunauté.

              <br/>  <span className="font-weight-bold">Donne un cours pour prendre un cours !</span>
            </p>
          </div>

          <div className="col-lg-10 d-flex p-4 m-auto align-items-center justify-content-center about-img">
            <img src="assets/img/devicesnew.png" className="img-fluid p-2" alt="" data-aos="zoom-in"/>
          </div>

        </div>
      </div>
      <div id="services" className="services section-bg">
        <div className="container">


          <div className="row">

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icongold"><i className='bx bx-log-in-circle' /></div>
                <h4 className="title"><a href="">1 - Inscris-toi facilement sur AKO</a></h4>
                <p className="description"> Rejoins la communauté d'entraide. Renseigne tes besoins et tes préférences d'apprentissage.
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Tu gagnes un AKOin. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icongold"><i className='bx bx-user-check'/></div>
                <h4 className="title"><a href="">2 - Effectue ta recherche simplement</a></h4>
                <p className="description">Trois matchs parfaits te seront instantanément proposés. Tu n'as plus qu'à choisir lequel te convient le mieux !​
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Une session coûte un AKOin. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icongold"><i className='bx bx-edit-alt'/></div>
                <h4 className="title"><a href="">3 - Apprends par l'expérience </a></h4>
                <p className="description">Utilise cette session privilégiée pour pratiquer avec l'aide d'un mentor adapté et bienveillant.
                  <br/><br/><br/><span className="font-weight-bold" style={{color: "#C5C5C5"}}> Ton AKOin lui est transféré. </span>
                </p>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
              <div className="icon-box">
                <div className="icongold"><i className='bx bx-user-voice' /></div>
                <h4 className="title"><a href="">4 - Transmets tes compétences</a></h4>
                <p className="description">Guide la communauté, bénéficie de nouvelles heures d'apprentissage et fais évoluer ton avatar !
                  <br/><br/><br/><span className="font-weight-bold align-items-end" style={{color: "#C5C5C5"}}> Récolte d'autres AKOins ! </span>
                </p>
              </div>


            </div>
          </div>
        </div>
      </div>
    </section>





  </Layout>
)
