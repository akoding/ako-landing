import React from 'react';
import Layout from '../components/Layout';
import SEO from '../components/SEO';

const seoData = {
  title: 'AKO - Cours collaboratifs',
  description: 'Cours collaboratifs entre particuliers',
  keywords: 'ako',
};

export default function FAQ() {
  return (
    <Layout>
      <SEO data={seoData} />

      <section id="faq" className="faq section-bg">
        <div className="container">

          <div className="section-title" data-aos="fade-up">
            <h2>F.A.Q</h2>
            <p>KézAKO ?</p>
          </div>

          <ul className="faq-list">

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" className="" href="#faq1">Que signifie AKO ?<i className="icofont-simple-up"/></a>
              <div id="faq1" className="collapse show" data-parent=".faq-list">
                <p>
                  Ako signifie apprendre et enseigner en maori.
                  <br/>Selon le concept d'ako:
                  <br/>- l'apprenant et l'enseignant peuvent tous les deux apporter des connaissances durant les interactions d'apprentissage
                  <br/>- de nouvelles connaissances naissent grâce à l'apprentissage partagé
                  <br/>- on n'attend pas des enseignants la maîtrise parfaite de tout
                  <br/>Nous partageons pleinement ces principes et nous voulons construire grâce à AKO une communauté d'apprentissage bienveillante et inclusive. Une communauté où la contribution de chacun sera valorisée. Une communauté où chacun pourra profiter des relations productives pour apprendre avec et auprès des autres membres.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" href="#faq2" className="collapsed">Les AKOins, qu'est ce que c'est ? Comment en gagner ?<i className="icofont-simple-up"/></a>
              <div id="faq2" className="collapse" data-parent=".faq-list">
                <p>
                  Sur AKO, il faut donner un cours pour prendre un cours. Lorsque vous aidez un membre de la communauté, vous obtenez des AKOins, monnaie virtuelle du site, équivalents à une heure. Vous pouvez ensuite "dépenser" ces AKOins pour apprendre et obtenir de l'aide à votre tour.
                  Vous pouvez aussi récolter des AKOins en rejoignant AKO parmi les premiers utilisateurs-testeurs ou si vous parrainez des amis.              </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" href="#faq3" className="collapsed">Comment être sûr que les mentors sélectionnés sont expérimentés ? <i className="icofont-simple-up"/></a>
              <div id="faq3" className="collapse" data-parent=".faq-list">
                <p>
                  Nous utilisons un système d'évaluation au sein d'AKO qui se base sur des avis reçus et sur l'activité des utilisateurs sur le site. Vous pouvez également regarder le profil de la personne, ses réalisations, et les projets sur lesquels il a aidé.
                  <br/>Notre mission est de vous aider en fonction de vos besoins spécifiques en vous mettant en contact avec la bonne personne. Si elle n'est pas encore inscrite dans la communauté, on vous indique les ressources gratuites ou payantes utiles pour résoudre le problème.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" href="#faq4" className="collapsed">Et si je veux obtenir un diplôme ? <i className="icofont-simple-up"/></a>
              <div id="faq4" className="collapse" data-parent=".faq-list">
                <p> Les sessions de formation sur AKO n'amènent pas au diplôme. Nous sommes là pour compléter les formations institutionnelles : profitez-en pour vous tester. Par la suite, vous pourrez vous orienter vers des formations plus complètes. Nous choisissons délibérément cette flexibilité pour vous permettre d'apprendre sur vos projets, rapidement et de manière pratique ainsi que pour fluidifier le partage des connaissances au sein de la société.              </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" href="#faq5" className="collapsed">Je ne suis pas expert, ni pédagogue. D'ailleurs, je n'ai aucune compétence à transmettre. Comment faire ?<i className="icofont-simple-up"/></a>
              <div id="faq5" className="collapse" data-parent=".faq-list">
                <p>
                  Chez AKO, nous sommes convaincus que chaque personne a des talents. Couture, cuisine, bricolage ? Toutes les compétences sont demandées par les membres de la communauté et chaque membre peut faire valoir ses compétences.
                  <br/>D'ailleurs, nous sommes persuadés qu'enseigner, c'est apprendre deux fois. Plus vous aidez les membres dans votre domaine de compétences et plus vous devenez un expert de ce domaine.
                  <br/>Nous progressons en pratiquant. Recevez le feedback constructif de vos élèves, soyez à l'écoute et adaptez-vous. Nous préparons pour vous des modules de formations pour vous aider à partager vos savoirs de manière claire et efficace. Alors qu'attendez vous ?
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <a data-toggle="collapse" href="#faq6" className="collapsed">J'aimerais contribuer au projet AKO, comment faire?  <i className="icofont-simple-up"/></a>
              <div id="faq6" className="collapse" data-parent=".faq-list">
                <p>
                  Dès le début du projet, nous co-construisons AKO avec ses utilisateurs potentiels. Nous sommes toujours à l'écoute de vos suggestions et serons ravis de profiter de votre aide occassionnelle pour avoir un retour sur nos idées et réalisations.
                  Si vous voulez échanger, n'hésitez pas à nous écrire sur hey.akoteam@google.com.
                  <br/>Si vous voulez contribuez de manière plus sérieuse et engagée au projet AKO, nous serions ravis d'étudier votre candidature.
                  <br/>Dans ce cas là, envoyez-nous votre CV et quelques lignes présentant vos motivations sur notre mail hey.akoteam@google.com en indiquant "Je veux rejoindre l'équipe AKO" dans l'objet de votre message.              </p>
              </div>
            </li>
            
          </ul>

        </div>
      </section>
    </Layout>
  );
}